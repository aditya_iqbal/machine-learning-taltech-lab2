#!/usr/bin/env python
import numpy as np
from sklearn.datasets import load_boston
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor
from sklearn.pipeline import make_pipeline
from matplotlib import pyplot as plt
from sklearn.pipeline import make_pipeline
from sklearn.metrics import mean_absolute_error


import common.feature_selection as feat_sel
import common.test_env as test_env


def print_metrics(y_true, y_pred, label):
    # Feel free to extend it with additional metrics from sklearn.metrics
    print('%s R squared: %.2f' % (label, r2_score(y_true, y_pred)))
    print('%s MSE score: %.2f' % (label, mean_absolute_error(y_true, y_pred)))
    print("-------------------------------------------")


def linear_regression(X, y, print_text='Linear regression all in'):
    # Split train test sets
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=42)

    # Linear regression all in
    reg = LinearRegression()
    reg.fit(X_train, y_train)
    y_pred = reg.predict(X_test)
    print_metrics(y_test, y_pred, print_text)


    # linreg = plt
    # linreg.figure()
    # linreg.scatter(y_test,y_pred)
    # linreg.xlabel('Y Test')
    # linreg.ylabel('Predicted Y')
    # linreg.savefig("results/Linear_regression.png")





    return reg


def linear_regression_selection(X, y):
    X_sel = feat_sel.backward_elimination(X, y)
    return linear_regression(X_sel, y, print_text='Linear regression with feature selection')


# STUDENT SHALL CREATE FUNCTIONS FOR POLYNOMIAL REGRESSION, SVR, DECISION TREE REGRESSION AND
# RANDOM FOREST REGRESSION

def polynomial_regression(X, y, print_text='Polynomial regression'):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=0)

    degree = 2
    polyreg = make_pipeline(PolynomialFeatures(degree),LinearRegression())
    polyreg.fit(X_train,y_train)
    print_metrics(y_test, polyreg.predict(X_test), print_text)


    


def svr(X, y, print_text='Support Vector Regression'):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=42)
    svr = SVR()
    svr.fit(X, y)

    y_pred = svr.predict(X_test)
    print_metrics(y_test, y_pred, print_text)

    pass


def decision_tree_regrssion(X, y, print_text='Decision Tree Regression'):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=42)
    dec_tree = DecisionTreeRegressor(max_depth=2)
    dec_tree.fit(X_train, y_train)
    y_pred = dec_tree.predict(X_test)
    print_metrics(y_test, y_pred, print_text)

    
    # dtr = plt.figure()
    # dtr.scatter(X, y, s=20, edgecolor="black",
    #         c="darkorange", label="data")
    # dtr.plot(X_test, y_1, color="cornflowerblue",
    #      label="max_depth=2", linewidth=2)
    # dtr.plot(X_test, y_2, color="yellowgreen", label="max_depth=5", linewidth=2)
    # dtr.xlabel("data")
    # dtr.ylabel("target")
    # dtr.title("Decision Tree Regression")
    # dtr.legend()
    # dtr.savefig("results/dec_tree_reg")
    # dtr.show()

    

def random_forest_regression(X,y, print_text = "Random forest regressiom"):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=0)
    rand_for = RandomForestRegressor()
    rand_for.fit(X_train, y_train)
    y_pred = rand_for.predict(X_train)
    # print('R square with random forest regression:',r2_score(y_train, y_pred))
    print_metrics(y_train, y_pred, print_text)

    # rfr = plt
    # rfr.figure()
    # rfr.scatter(y_train, y_pred)
    # rfr.xlabel("Prices")
    # rfr.ylabel("Predicted prices")
    # rfr.title("Prices vs Predicted prices Random Forest Regression")
    # rfr.savefig('results/rand_forest.png')
    # rfr.show()
    # return rand_for

if __name__ == '__main__':
    test_env.versions(['numpy', 'statsmodels', 'sklearn'])

    # https://scikit-learn.org/stable/datasets/index.html#boston-house-prices-dataset
    X, y = load_boston(return_X_y=True)
    print(X.shape)
    print(y.shape)

    linear_regression(X, y)
    linear_regression_selection(X, y)
    polynomial_regression(X, y)
    svr(X, y)
    decision_tree_regrssion(X,y)
    random_forest_regression(X, y)
    

    # STUDENT SHALL CALL POLYNOMIAL REGRESSION, SVR, DECISION TREE REGRESSION AND
    # RANDOM FOREST REGRESSION FUNCTIONS

    print('Done')
